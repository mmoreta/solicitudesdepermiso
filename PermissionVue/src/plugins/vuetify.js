import Vue from 'vue'
import {
  Vuetify,
  VApp,
  VNavigationDrawer,
  VFooter,
  VList,
  VBtn,
  VIcon,
  VGrid,
  VToolbar,
  VCard,
  VDatePicker,
  VDivider,
  VDialog,
  VTextField,
  VDataTable,
  VSelect,
  VMenu,
  transitions,
  VChip,
  VAvatar,
  VAlert,
  VAutocomplete
} from 'vuetify'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  components: {
    VApp,
    VNavigationDrawer,
    VFooter,
    VList,
    VBtn,
    VIcon,
    VGrid,
    VToolbar,
    VCard,
    VDatePicker,
    VMenu,
    VDivider,
    VDialog,
    VTextField,
    VDataTable,
    VSelect,
    transitions,
    VChip,
    VAvatar,
    VAlert,
    VAutocomplete
  },
  theme: {
    primary: '#1976D2',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  },
})
