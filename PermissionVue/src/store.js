import Vue from 'vue'
import Vuex from 'vuex'
import decode from 'jwt-decode'
import router from './router'
import { stat } from 'fs'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: null,
    user: null,
    role: null
  },
  mutations: {
    setToken(state,token){
      state.token=token      
    },
    setUsuario(state,user){
      state.user=user
    }
  },
  actions: {
    guardarToken({commit},token){
      commit("setToken", token.token)
      commit("setUsuario", decode(token.token))
      localStorage.setItem("token", token.token)
      localStorage.setItem("role", token.user.role.name)
      localStorage.setItem("user", token.user.id)
    },
    autoLogin({commit}){
      let token = localStorage.getItem("token")
      if (token){
        commit("setToken", token)
        commit("setUsuario", decode(token))
      }
      router.push({name: 'home'})
    },
    salir({commit}){
      commit("setToken", null)
      commit("setUsuario", null)
      localStorage.removeItem("token")
      localStorage.removeItem("role")
      localStorage.removeItem("user")
      router.push({name: 'login'})
    }
  },
  getters: {
    rutas(state){
      return localStorage.getItem("role")
    }
  }
})
