import Vue from 'vue'
import Router from 'vue-router'
import Login from './components/Login.vue'
import Home from './views/Home.vue'
import Usuario from './components/Usuario.vue'
import Rol from './components/Rol.vue'
import noAuthorize from './components/Authorize/noAuthorize.vue'
import store from './store'

Vue.use(Router)

var router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta :{
        administrador :true,
        supervisor :true,
        encargado: true
      }
    },
    {
      path: '/roles',
      name: 'roles',
      component: Rol,
      meta :{
        administrador :true
      }
    },
    {
      path: '/usuarios',
      name: 'usuarios',
      component: Usuario,
      meta :{
        administrador :true
      }
    },  
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta : {
        libre: true
      }
    },
    {
      path: '/noAuthorize',
      name: 'noAuthorize',
      component: noAuthorize,
      meta : {
        libre: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.libre)){
    next()
  } else if (store.state.user && store.getters.rutas == 'Administrador'){
    if (to.matched.some(record => record.meta.administrador)){
      next()
    }
  } else if (store.state.user && store.getters.rutas == 'Encargado'){
    if (to.matched.some(record => record.meta.encargado)){
      next()
    }
  } else if (store.state.user && store.getters.rutas == 'Supervisor'){
    if (to.matched.some(record => record.meta.supervisor)){
      next()
    }
  } else{
    next({
      name: 'login'
    })
  }
})

export default router
