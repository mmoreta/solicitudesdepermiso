﻿using Control.Models.IAudithInterfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Control.Models.EntityModels.GenericModel
{
    public class GenericModel : IGenericModel
    {
        [Key]
        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public bool Active { get; set; }

        #region Implementing Interface

        public int CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedDate { get; set; }

        #endregion
    }
}
