﻿using System.ComponentModel.DataAnnotations;

namespace Control.Models.EntityModels
{
    public class User : GenericModel.GenericModel
    {
        [MaxLength(100)]
        public string FirstName { get; set; }
        [MaxLength(100)]
        public string LastName { get; set; }
        [MaxLength(15)]
        public string Username { get; set; }
        [MaxLength(200)]
        public string Email { get; set; }

        public int RoleId { get; set; }

        //public string FullName
        //{
        //    get { return FirstName + " " + LastName; }
        //}
        
        public Role Role { get; set; }
    }
}
