﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Control.Models.EntityModels
{
    public class Permission : GenericModel.GenericModel
    {
        public string EmployeeName { get; set; }
        public string EmployeeLastName { get; set; }
        public DateTime PermissionDate { get; set; }

        [ForeignKey("PermissionType")]
        public int PermissionTypeId { get; set; }
        public PermissionType PermissionType { get; set; }
    }
}
