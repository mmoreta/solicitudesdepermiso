﻿using System.Collections.Generic;

namespace Control.Models.EntityModels
{
    public class PermissionType : GenericModel.GenericModel
    {
        public string Description { get; set; }
        public virtual ICollection<Permission> Permissions { get; set; }
    }
}
