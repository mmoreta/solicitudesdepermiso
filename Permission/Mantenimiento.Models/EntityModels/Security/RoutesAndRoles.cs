﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Control.Models.EntityModels
{
    public class RoutesAndRoles : GenericModel.GenericModel
    {
        public string RouteName { get; set; }
        public string RouteUrl { get; set; }
        public string ComponentRoute { get; set; }
        public string RoleName { get; set; }
    }
}
