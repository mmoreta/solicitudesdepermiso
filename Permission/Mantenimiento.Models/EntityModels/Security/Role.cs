﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Control.Models.EntityModels
{
    public class Role : GenericModel.GenericModel
    {
        [Required]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "El nombre no debe de tener más de 30 caracteres, ni menos de 3 caracteres.")]
        public string Name { get; set; }
        [StringLength(256)]
        public string Description { get; set; }
        public bool Status { get; set; }

       // public virtual List<User> Users { get; set; }
    }
}
