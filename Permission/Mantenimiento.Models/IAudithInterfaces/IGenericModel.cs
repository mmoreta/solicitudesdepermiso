﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Control.Models.IAudithInterfaces
{
    public interface IGenericModel
    {
        [Key]
        int Id { get; set; }
    }
}
