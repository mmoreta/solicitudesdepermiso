﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Control.Models.IAudithInterfaces
{
    public interface IAudithInterfaces
    { 
        int CreatedBy { get; set; }
        DateTime? CreationDate { get; set; }
        int UpdatedBy { get; set; }
        DateTime? UpdatedDate { get; set; }
        bool? IsDeleted { get; set; }
        DateTime? DeletedDate { get; set; }        
    }
}
