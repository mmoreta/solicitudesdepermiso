﻿using Control.Models.EntityModels;
using Microsoft.EntityFrameworkCore;

namespace Control.Models.Context
{
    public class PermissionDbContext : DbContext
    {
        public PermissionDbContext()
        {
        }
        public PermissionDbContext(DbContextOptions<PermissionDbContext> options)
           : base(options)
        {
        }

        DbSet<User> Users { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<RoutesAndRoles> RoutesAndRoles { get; set; }
        DbSet<Permission> Permissions { get; set; }
        DbSet<PermissionType> PermissionTypes { get; set; }
        


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {            
        }
    }
}

