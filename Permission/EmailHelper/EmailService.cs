﻿using EmailHelper.services;
using Microsoft.Exchange.WebServices.Data;
using System;

namespace EmailHelper
{
    public class EmailService
    {
        public EmailService()
        {
        }
        public void ExchangeUserNotification(string userName, string userPassWord, PlantillaHtml plantillaFile, string emails)
        {
     
            try
            {

                var senderAddress = "";
                var plantilla = plantillaFile;
                var mensaje = plantilla.Formatear(userName, userPassWord);

                var service = new ExchangeService(ExchangeVersion.Exchange2013);
                service.Credentials = new WebCredentials("noresponder@one.gob.do", "AS3p7v11", "192.168.1.41:25");
                service.Url = new Uri("https://mail.one.gob.do/ews/Exchange.asmx");

                service.UseDefaultCredentials = true;

               // service.Credentials = new NetworkCredential(senderAddress, senderPassword, mailDomain);
                EmailMessage message = new EmailMessage(service);
                message.Subject = "Control de suministro ONE";
                message.Body = mensaje;
                message.Sender = senderAddress;
                message.ToRecipients.Add("miguel.moreta@one.gob.do");
                message.ToRecipients.Add("daniel.mejia@one.gob.do");

                message.Save();
                message.SendAndSaveCopy();

            }
            catch (Exception ex)
            {
                var inner = ex.InnerException as System.Net.Sockets.SocketException;
                var errorCode = inner.ErrorCode;
                Console.WriteLine(inner.ErrorCode);
                Console.WriteLine(inner.Message);
            }

        }
        public void ExchangEquipmentNotification(string userName, string userPassWord, PlantillaHtml plantillaFile, string emails)
        {
            try
            {
                var senderAddress = "Control de suministro ONE";

                var plantilla = plantillaFile;
                var mensaje = plantilla.PlantillaSimple();

                var service = new ExchangeService(ExchangeVersion.Exchange2013);
                service.Credentials = new WebCredentials("noresponder@one.gob.do", "AS3p7v11", "192.168.1.41:25");
                service.Url = new Uri("https://mail.one.gob.do/ews/Exchange.asmx");

                service.UseDefaultCredentials = true;

                EmailMessage message = new EmailMessage(service);
                message.Subject = "Control de suministro ONE";
                message.Body = mensaje;
                message.Sender = senderAddress;
                message.ToRecipients.Add("miguel.moreta@one.gob.do");
                //message.ToRecipients.Add("daniel.mejia@one.gob.do");

                message.Save();
                message.SendAndSaveCopy();

            }
            catch (Exception ex)
            {
                var inner = ex.InnerException as System.Net.Sockets.SocketException;
                var errorCode = inner.ErrorCode;
                Console.WriteLine(inner.ErrorCode);
                Console.WriteLine(inner.Message);
            }

        }
    }
}
