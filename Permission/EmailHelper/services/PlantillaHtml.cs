﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace EmailHelper.services
{
    public class PlantillaHtml
    {
        private string _html;

        public PlantillaHtml(string localizacionPlantilla)
        {
            using (var lector = new StreamReader(localizacionPlantilla))
                _html = lector.ReadToEnd();
        }

        public string Formatear(string userName, string passWord)
        {
            string salida = _html;
         
            salida = salida.Replace("[UserName]", (userName as string) ?? string.Empty);
            salida = salida.Replace("[UserPassword]", (passWord as string) ?? string.Empty);

            return salida;
        }

        public string PlantillaSimple()
        {
            string salida = _html;

            return salida;
        }
    }
}
