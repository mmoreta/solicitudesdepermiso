﻿using Control.Models.EntityModels;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;


namespace Control.Services
{
    public class TokenGeneratorService : ITokenGeneratorService
    {
        private readonly JwtConfig _config;

        public TokenGeneratorService(IOptions<JwtConfig> config)
        {
            _config = config.Value;
        }

        public object GenerateJwtToken(string name, User user)
        {
            var claims = new List<Claim>
            {
                //new Claim(ClaimTypes.GivenName, name?.Split(' ')[0]),
                //new Claim(ClaimTypes.Name, name),
                //new Claim(ClaimTypes.Sid, user.Id.ToString()),
                //new Claim(ClaimTypes.NameIdentifier, user.Username),
                //new Claim(ClaimTypes.Email, user.Email ?? string.Empty),
                //new Claim(ClaimTypes.Sid, Guid.NewGuid().ToString()),
                //new Claim(ClaimTypes.Role, user.Role.Name ),


                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Email, user.Email ?? string.Empty),
                new Claim(ClaimTypes.Role, user.Role.Name ),
                new Claim("Id", user.Id.ToString() ),
                new Claim("Role", user.Role.Name ),
                new Claim("Username", user.Username )
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.Key));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddMinutes(30);

            var token = new JwtSecurityToken(
                _config.Issuer,
                _config.Issuer,
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
