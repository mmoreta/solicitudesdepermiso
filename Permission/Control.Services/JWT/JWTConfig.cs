﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Control.Services
{
    public class JwtConfig
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
        public int ExpireDays { get; set; }
    }
}
