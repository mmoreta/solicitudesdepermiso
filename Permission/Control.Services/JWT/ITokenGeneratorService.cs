﻿using Control.Models.EntityModels;

namespace Control.Services
{
    public interface ITokenGeneratorService
    {
        object GenerateJwtToken(string givenName, User user);
    }
}
