﻿using Control.Common.Repository;
using Control.Models.Context;
using Control.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Control.Services.Repositories
{
   public class RoleRepository : GenericRepository<Role>
    {
        public RoleRepository(PermissionDbContext _context) : base(_context)
        { 
        }
    }
}
