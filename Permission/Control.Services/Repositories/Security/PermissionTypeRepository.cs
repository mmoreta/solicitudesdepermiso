﻿using Control.Common.Repository;
using Control.Models.Context;
using Control.Models.EntityModels;

namespace Control.Services.Repositories
{
    public class PermissionTypeRepository : GenericRepository<PermissionType>
    {
        public PermissionTypeRepository(PermissionDbContext _context) : base(_context)
        { 
        }
    }
}
