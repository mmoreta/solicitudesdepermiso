﻿using Control.Common.Repository;
using Control.Models.Context;
using Control.Models.EntityModels;

namespace Control.Services.Repositories
{
    public class PermissionRepository : GenericRepository<Permission>
    {
        public PermissionRepository(PermissionDbContext _context) : base(_context)
        { 
        }
    }
}
