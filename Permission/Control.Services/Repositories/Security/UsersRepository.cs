﻿using Control.Common.Repository;
using Control.Models.Context;
using Control.Models.EntityModels;

namespace Control.Services.Repositories
{
    public class UsersRepository : GenericRepository<User>
    {
        public UsersRepository(PermissionDbContext _context) : base(_context)
        { 
        }
    }
}
