﻿using AutoMapper;
using Control.Models.Context;
using Control.Services;
using Control.Services.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using UI.Api.Mappers.MapperProfiles;

namespace UI.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
 
            services.AddMvcCore()
            .AddApiExplorer();
            string conString = ConfigurationExtensions
              .GetConnectionString(this.Configuration, "DefaultConnection");

            services.AddDbContext<PermissionDbContext>
                (options => options.UseSqlServer(conString));

            services.AddAutoMapper(typeof(MappingProfile));

            services.AddTransient<UsersRepository, UsersRepository>();
            services.AddTransient<RoleRepository, RoleRepository>();

            // Permission
            services.AddTransient<PermissionRepository, PermissionRepository>();
            services.AddTransient<PermissionTypeRepository, PermissionTypeRepository>();

            //AD
            services.AddTransient<IADUserManagerService, ADUserManagerService>();
            services.Configure<AdConfig>(Configuration.GetSection(typeof(AdConfig).Name));

            //JWT
            services.AddTransient<ITokenGeneratorService, TokenGeneratorService>();
            services.Configure<JwtConfig>(Configuration.GetSection(typeof(JwtConfig).Name));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Control de permisos por tipo",
                    Description = "WEB API - Para consumo interno de la Empresa X",
                    TermsOfService = "None",
                    Contact = new Contact() { Name = "Miguel Moreta", Email = "miguel@mmoreta.com", Url = "https://mmoreta.com/cv" }
                });
            });

            services.AddCors(options => {
                options.AddPolicy("default",
                builder => builder.WithOrigins("*").WithHeaders("*").WithMethods("*"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseCors("default");
            app.UseMvc();
        }
    }
}