﻿using AutoMapper;
using Control.Common.Interfaces;
using Control.Models.EntityModels.GenericModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UI.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class BaseApiController<T, TD> : ControllerBase
      where T : GenericModel
      where TD : class
    {
        //protected readonly GenericRepository<T> _repository;
        private readonly IGenericRepository<T> _repository = null;

        protected readonly IMapper _mapper;

        // Assign the object in the constructor for dependency injection
        public BaseApiController(IMapper mapper, IGenericRepository<T> repository)
        {
            _repository = repository;
            _mapper = mapper;
        }

        // GET: api/Resident
        [HttpGet]
        public List<T> Get()
        {
            var result = _repository.GetAll().Where(x => x.IsDeleted != true);
            return result.ToList();
        }

        // GET: api/Resident/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var objectEntity = _repository.Find(x => x.Id == id && x.IsDeleted != true).FirstOrDefault(x => x.Id == id);

            return Ok(objectEntity);
        }

        // POST: api/Resident
        [HttpPost]
        public void Post([FromBody] TD headLine)
        {
            var resource = _mapper.Map<T>(headLine);
            resource.IsDeleted = false;
            resource.CreationDate = DateTime.Now;
            resource.Active = true;

            _repository.Add(resource);
            _repository.Save();
        }

        // PUT: api/Resident/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] TD dto)
        {
            var model = _repository.Find(x => x.Id == id && x.IsDeleted != true).FirstOrDefault(x => x.Id == id);
                        
            model = _mapper.Map(dto, model);
            model.Id = id;
            model.UpdatedDate = DateTime.Now;
            model.UpdatedBy = id;

            _repository.Edit(model);
            _repository.Save();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var objectEntity = _repository.Find(x => x.Id == id && x.IsDeleted != true).Single();
            var resource = _mapper.Map<T>(objectEntity);
            resource.IsDeleted = true;
            resource.DeletedDate = DateTime.Now;
            resource.UpdatedDate = DateTime.Now;
            resource.UpdatedBy = id;
            
            _repository.Edit(resource);
            _repository.Save();
        }
    }
}
