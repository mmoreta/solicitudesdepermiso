﻿using AutoMapper;
using Control.Models.EntityModels;
using Control.Services.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UI.Api.Controllers;
using UI.Api.Mappers;

namespace ControlVisitas.Controllers
{
    public class UsersController : BaseApiController<User, UsersDto>
    {
        protected readonly UsersRepository _repository;

        public UsersController(IMapper mapper, UsersRepository repository) : base(mapper, repository)
        {
            _repository = repository;
        }

        // GET: api/UsersWithRoles/
        // POST: api/Product/GetProducts
        [HttpGet("[action]")]
        public async Task<IEnumerable<UsersDto>> GetUsersWithRole()
        {
            var result = await _repository.GetAll()
                                            .Where(a => a.IsDeleted != true && a.Active != false)
                                            .Include(i => i.Role)
                                            .OrderBy(i => i.Id)
                                            .Take(100)
                                            .ToListAsync();

            return _mapper.Map<IEnumerable<UsersDto>>(result);
        }
    }
}
