﻿using AutoMapper;
using Control.Models.EntityModels;
using Control.Services.Repositories;
using UI.Api.Mappers.Dto;

namespace UI.Api.Controllers
{
    //[Authorize]
    public class PermissionTypeController : BaseApiController<PermissionType, PermissionTypeDto>
    {
        public PermissionTypeController(IMapper mapper, PermissionTypeRepository repository) : base(mapper, repository)
        {
        }
    }
}