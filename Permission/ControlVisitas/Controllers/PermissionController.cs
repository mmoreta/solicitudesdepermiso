﻿using AutoMapper;
using Control.Models.EntityModels;
using Control.Services.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UI.Api.Mappers.Dto;

namespace UI.Api.Controllers
{
    //[Authorize]
    public class PermissionController : BaseApiController<Permission, PermissionDto>
    {
        protected readonly PermissionRepository _repository;

        public PermissionController(IMapper mapper, PermissionRepository repository) : base(mapper, repository)
        {
            _repository = repository;
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<PermissionDto>> GetPermissionsWithTypes()
        {
            var result = await _repository.GetAll()
                                            .Where(a => a.IsDeleted != true && a.Active != false)
                                            .Include(i => i.PermissionType)
                                            .OrderBy(i => i.Id)
                                            .Take(100)
                                            .ToListAsync();

            return _mapper.Map<IEnumerable<PermissionDto>>(result);
        }
    }
}