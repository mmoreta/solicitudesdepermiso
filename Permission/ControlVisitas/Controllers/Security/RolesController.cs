﻿using AutoMapper;
using Control.Models.EntityModels;
using Control.Services.Repositories;
using ControlVisitas.Controllers;
using Microsoft.AspNetCore.Authorization;
using UI.Api.Mappers;

namespace UI.Api.Controllers
{
    //[Authorize]
    public class RolesController : BaseApiController<Role, RolesDto>
    {
        public RolesController(IMapper mapper, RoleRepository repository) : base(mapper, repository)
        {
        }
    }
}