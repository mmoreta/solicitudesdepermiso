﻿using AutoMapper;
using Control.Services;
using Control.Services.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using UI.Api.Mappers;

namespace UI.Api.Controllers
{
    [Route("AutenticacionAdmin")]
    public class AuthAdminController : Controller
    {
        private readonly IADUserManagerService _adUserManagerService;
        private readonly ITokenGeneratorService _tokenGeneratorService;
        private readonly IMapper _mapper;
        private readonly UsersRepository _repository;

        public AuthAdminController(IADUserManagerService adUserManagerService, UsersRepository repository, 
                                   ITokenGeneratorService tokenGeneratorService, IMapper mapper)
        {
            _adUserManagerService = adUserManagerService;
            _tokenGeneratorService = tokenGeneratorService;
            _mapper = mapper;
            _repository = repository;
        }


        [HttpPost("InicioSesion")]
        public IActionResult Login([FromBody] LoginDto model)
        {
            var usuario = _repository.Find(a => a.Active == true && a.IsDeleted == false)
                .Include(r => r.Role)
                .FirstOrDefault(a => a.Username == model.Username);

            if (usuario == null)
            {
                return Ok("Usuario no valido");
            }

            if (!usuario.Active)
            {
                return Ok("Usuario inactivo");
            }

            bool isValid = _adUserManagerService.ValidateCredentials(model.Username, model.Password);

            if (!isValid)
            {
                return Ok("Usuario o contrasena incorrecto");
            }
                        
            var payload = new
            {
                token = _tokenGeneratorService.GenerateJwtToken($"{usuario.FirstName} {usuario.LastName}", usuario),
                user = _mapper.Map<UsersDto>(usuario),
                role = usuario.Role.Name
                //allowedModules = permissions
            };

            return Json(payload);
        }
               
    }
}