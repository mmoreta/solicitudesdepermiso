﻿using AutoMapper;
using Control.Models.EntityModels;
using UI.Api.Mappers.Dto;

namespace UI.Api.Mappers.MapperProfiles
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UsersDto>().ReverseMap();
            CreateMap<Role, RolesDto>().ReverseMap();
            CreateMap<Permission, PermissionDto>().ReverseMap();
            CreateMap<PermissionType, PermissionTypeDto>().ReverseMap();
        }
    }
}
