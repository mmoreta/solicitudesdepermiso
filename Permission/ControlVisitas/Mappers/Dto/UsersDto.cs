﻿using Control.Models.EntityModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UI.Api.Mappers
{
    public class UsersDto
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string FirstName { get; set; }
        [MaxLength(100)]
        public string LastName { get; set; }
        [MaxLength(15)]
        public string Username { get; set; }
        [MaxLength(200)]
        public string Email { get; set; }
        public bool Active { get; set; }

        public int RoleId { get; set; }
        [ForeignKey("Id")]
        public virtual Role Role { get; set; }
    }
}
