﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UI.Api.Mappers.Dto
{
    public class PermissionDto
    {
        public int Id { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "The employee name cannot be empty")]
        public string EmployeeName { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "The last name field cannot be empty")]
        public string EmployeeLastName { get; set; }

        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "The date field cannot be empty")]
        public DateTime PermissionDate { get; set; }

        [Required(ErrorMessage = "Permission field cannot be empty")]
        public int PermissionTypeId { get; set; }
        public virtual PermissionTypeDto PermissionType { get; set; }
    }
}
