﻿using System.ComponentModel.DataAnnotations;

namespace UI.Api.Mappers.Dto
{
    public class PermissionTypeDto
    {
        public int Id { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "A description is required")]
        public string Description { get; set; }
    }
}
