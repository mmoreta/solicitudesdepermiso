﻿using Control.Common.Interfaces;
using Control.Models.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Control.Common.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected readonly PermissionDbContext _context;
        private readonly DbSet<T> _set;

        public GenericRepository(PermissionDbContext context)
        {
            _context = context;
            _set = _context.Set<T>();
        }

        public void Add(T entity)
        {
            _set.Add(entity);
        }

        public void Delete(T entity)
        {
            _set.Remove(entity);
        }

        public void Edit(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        public IQueryable<T> Find(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = _set.Where(predicate);
            return query;
        }

        public System.Linq.IQueryable<T> GetAll()
        {
            IQueryable<T> query = _set;
            return query;
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
